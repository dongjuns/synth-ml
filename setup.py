from setuptools import setup

setup(
    name='synth_ml',
    version='0.0.1',
    scripts=[
        'bin/synth-ml-blender',
    ],
    install_requires=[
        'progress',
        'pyquery',
        'numpy',
        'wget',
        'transformations',
        'scipy'
    ],
)
