import cv2
from pathlib import Path
from synth_ml.blender.callbacks.metadata import Metadata

Path("center_marked").mkdir(exist_ok=True)
for i in range(10):
    md = Metadata("synth_ml_data/metadata/{:04}.json".format(i))
    bgr = cv2.imread("synth_ml_data/cycles_denoise/{:04}.png".format(i))
    center = md.world_2_image(md.objects['Suzanne'].position)
    cv2.drawMarker(bgr, tuple(center[:2]), (0, 0, 255), cv2.MARKER_CROSS, 15, 2)
    cv2.imwrite("center_marked/{:04}.png".format(i), bgr)
