import cv2
import trimesh
from pathlib import Path
from synth_ml.blender.callbacks.metadata import Metadata
from synth_ml.objs import get_objs_folder

obj = trimesh.load(get_objs_folder() / "suzanne.obj")  # type: trimesh.Trimesh
vertices = obj.vertices.T

Path("bbox_marked").mkdir(exist_ok=True)
for i in range(10):
    md = Metadata("synth_ml_data/metadata/{:04}.json".format(i))
    xmin, ymin, xmax, ymax = md.bbox(vertices, md.objects['Suzanne'].t_world)
    bgr = cv2.imread("synth_ml_data/cycles_denoise/{:04}.png".format(i))
    cv2.rectangle(bgr, (xmin, ymin), (xmax, ymax), (0, 0, 255), 2)
    cv2.imwrite("bbox_marked/{:04}.png".format(i), bgr)
