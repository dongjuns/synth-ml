from synth_ml.blender.wrappers import Scene, Camera, Object
from synth_ml.blender import callbacks as cb, materials
from synth_ml.objs import get_objs_folder
from datetime import datetime
import yaml

monthDay = datetime.today().strftime("%m%d")

s = Scene()

cam = Camera(scene=s, location=(0, 0.395, 0.147), focal_length=8.5, sensor_width= 9.2, clip_start=0.1, clip_end=100).look_at((0, 0, 0.062))

# ~/synth-ml/synth-ml/blender/wrappers/object.py
obj2 = Object.import_obj(get_objs_folder() / "subtask_D.obj", scene=s)[0]
obj = Object.import_obj(get_objs_folder() / "pulley.obj", scene=s)[0]

# ~/synth-ml/synth-ml/blender/wrappers/material.py
# randomize the matrial
#material = materials.ProceduralNoiseVoronoiMix()
material = materials.UniformMaterial()
obj.assign_material(material)
obj2.assign_material(material)

# ~/synth-ml/synth-ml/blender/callbacks/transform_sampler.py
ts = cb.TransformSampler()

# case1. correctly assembled motor pulley: 1 mm
# r_min = 0, r_max = 0.001
# case2. missing motor pulley
# case3. too far in: -3.5 mm < z < -1 mm.
# r_min = -0.001, r_max = -0.0035
# case4. too far out: 1 mm < z < 3.5 mm.
# r_min = 0.001, r_max = 0.0035

subtaskType = "subtask_B"

caseType = "case1"
#caseType = "case2"
#caseType = "case3"
#caseType = "case4"

if caseType == "case1":
    ts.pos_axis_uniform(obj, axis="z", mi=-0.001, ma=0.001)
elif caseType == "case2":
    ts.pos_axis_uniform(obj, axis="z", mi=10, ma=11)
elif caseType == "case3":
    ts.pos_axis_uniform(obj, axis="z", mi=-0.0035, ma=-0.001)
elif caseType == "case4":
    ts.pos_axis_uniform(obj, axis="z", mi=0.001, ma=0.0035)

# Rotate the position of the camera
#ts.rot_axis_uniform(cam, 'z', -.3, .3)
#ts.rot_axis_uniform(cam, 'z', 0, 0)

# ~/synth-ml/synth-ml/blender/callbacks/renderer.py
# resolution = (x, y)
# HdriEnvironment: change the list of the hdri_haven_meta_data.json
# ~/synth-ml/synth-ml/blender/callbacks/hdri_haven_meta_data.json
s.callback = cb.CallbackCompose(
    cb.Renderer(
        scene=s, resolution=(1920,1200), engines='CYCLES', cycles_samples=0,
        denoise=True, depth=False, normal=False, taskName=subtaskType, dateTime=monthDay, directoryName=caseType
    ),
    cb.MetadataLogger(scene=s, objects=[obj]),
    ts,
    material.sampler_cb(p_metallic=1.0, roughness=0.45),
    cb.HdriEnvironment(scene=s),
)

s.render_frames(range(5))
