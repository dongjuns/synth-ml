from synth_ml.blender.wrappers import Scene, Camera, Object
from synth_ml.blender import callbacks as cb, materials
from synth_ml.objs import get_objs_folder
from datetime import datetime
import yaml
import os

monthDay = datetime.today().strftime("%m%d")
subtaskPath = os.getcwd()
subtaskFile = "subtask_B.yaml"

with open(os.path.join(subtaskPath, subtaskFile), "r") as stream:
    data_loaded = yaml.safe_load(stream)

for data in data_loaded:
    s = Scene()
    obj = Object.import_obj(get_objs_folder() / "pulley.obj", scene=s)[0]
    obj2 = Object.import_obj(get_objs_folder() / "subtask_B.obj", scene=s)[0]
    
    cam = Camera(scene=s,
                 location=data_loaded[data]["center"],
                 focal_length=data_loaded["case1"]["focal_length"],
                 sensor_width=data_loaded["case1"]["sensor_width"],
                 clip_start=data_loaded["case1"]["clip_start"],
                 clip_end=data_loaded["case1"]["clip_end"]).look_at((0, 0, 0.062))

    #material = materials.ProceduralNoiseVoronoiMix()
    material = materials.UniformMaterial()
    obj.assign_material(material)
    obj2.assign_material(material)

    subtaskType = subtaskFile.split(".")[0]
    caseType = data_loaded[data]["name"]
    ts = cb.TransformSampler()
    ts.pos_axis_uniform(obj,
                        axis=data_loaded["case1"]["axis"],
                        mi=data_loaded[data]["r_min"],
                        ma=data_loaded[data]["r_max"])

    s.callback = cb.CallbackCompose(
        cb.Renderer(
            scene=s, resolution=(1920,1200), engines='CYCLES', cycles_samples=10,
            denoise=True, depth=False, normal=False, taskName = subtaskType, dateTime=monthDay, directoryName=caseType
        ),
        cb.MetadataLogger(scene=s, objects=[obj]),
        ts,
        material.sampler_cb(p_metallic=data_loaded["case1"]["p_metallic"], roughness=data_loaded["case1"]["roughness"]),
        cb.HdriEnvironment(scene=s),
    )
    s.render_frames(range(5))
