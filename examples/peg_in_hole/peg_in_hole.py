import argparse

from synth_ml.utils import rand
from synth_ml.blender import bl_utils, materials
from synth_ml.blender.wrappers import Scene, Object, Camera
import synth_ml.blender.callbacks as cb

parser = argparse.ArgumentParser()
parser.add_argument("--frame-start", type=int, default=0)
parser.add_argument("--frame-end", type=int, default=1000)
args = parser.parse_args(bl_utils.get_user_args())

s = Scene.from_name('Scene')
ts = cb.TransformSampler()

cube = Object.from_name('Cube')
peg = Object.from_name('Peg')

material = materials.ProceduralNoiseVoronoiMix()
cube.assign_material(material)
peg.assign_material(material)

target = Object(scene=s)
ts.pos_sphere_volume(obj=target, center=(0, 0.1, 0), radius=0.05)
tripod = Object(scene=s).look_at(target)
ts.pos_sphere_volume(obj=tripod, center=(0, -.2, 0), radius=0.1)

cam = Camera(scene=s, parent=tripod)
ts.rot_axis_uniform(cam, 'z', -1, 1)

s.callback = cb.CallbackCompose(
    ts,
    material.sampler_cb(scale=100, p_metallic=0.8, roughness=rand.NormalFloatSampler(0.1, .5, mi=0, ma=1)),
    cb.HdriEnvironment(scene=s, category='all', resolution=1, max_altitude_deg=0),
    cb.MetadataLogger(scene=s),
    cb.Renderer(
        scene=s, resolution=256, engines=('CYCLES',), denoise=True,
        cycles_samples=rand.NormalFloatSampler(mu=0, std=20, mi=5, ma=20, round=True),
    ),
)

s.render_frames(range(args.frame_start, args.frame_end))
