from pathlib import Path
import wget
import tarfile
import hashlib
import platform


def get_downloads_dir():
    downloads_dir = Path(__file__).parent.parent / 'downloads'
    if not downloads_dir.exists():
        downloads_dir.mkdir()
    return downloads_dir


def compute_md5(filepath):
    h = hashlib.md5()
    with open(filepath, 'rb') as f:
        while True:
            fb = f.read(int(1e6))
            if len(fb) == 0:
                break
            h.update(fb)
    return h.hexdigest()


def safe_download(url, md5):
    downloads_dir = get_downloads_dir()
    filename = url.split('/')[-1]
    filepath = downloads_dir / filename
    filepath_ok = filepath.parent / (filepath.name + '.md5ok')
    if filepath_ok.exists():
        return filepath
    if filepath.exists():
        filepath.unlink()
    print('Downloading', url)
    wget.download(url, str(filepath))
    print('\nChecking md5', md5)
    assert compute_md5(filepath) == md5
    filepath_ok.touch()
    return filepath


def extract(comp_path):
    extracted_folder = comp_path.parent / (comp_path.name + '.extracted')
    if not extracted_folder.exists():
        print('Extracting', comp_path, '...')
        tf = tarfile.open(comp_path)
        tf.extractall(path=str(extracted_folder))
    return extracted_folder


def get_blender_path(download=False):
    if platform.system() == "Linux":
        blender_path = get_downloads_dir() / 'blender-2.81a-linux-glibc217-x86_64.tar.bz2.extracted' / \
                       'blender-2.81a-linux-glibc217-x86_64' / 'blender'
        if blender_path.exists():
            return blender_path
        if not download:
            raise Exception('Blender is not downloaded')
        url = 'https://mirrors.dotsrc.org/blender/blender-release/Blender2.81/blender-2.81a-linux-glibc217-x86_64.tar.bz2'
        md5 = 'bb6e03ef79d2d7273336f8cfcd5a3b3f'
        blender_compressed_path = safe_download(url, md5)
        extract(blender_compressed_path) / url.split('/')[-1][:-8]
        return blender_path
    elif platform.system() == "Darwin":
        blender_contents_path = Path("/Applications/Blender.app/Contents")
        assert blender_contents_path.exists(), "Found no blender install. Please install blender 2.81a"
        info = "\n".join((blender_contents_path / "Info.plist").open().readlines())
        assert "2.81a" in info, "Found blender install, but it's not 2.81a. Please install blender 2.81a."
        blender_path = blender_contents_path / "MacOS" / "Blender"
        return blender_path
    else:
        assert False, "This platform is not currently supported"


if __name__ == '__main__':
    import subprocess

    blender = get_blender_path()
    subprocess.Popen(str(blender)).wait()
