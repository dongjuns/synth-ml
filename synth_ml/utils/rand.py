import random
from random import random as r
import math
from numbers import Number
from typing import Union, List, Tuple, Callable

import numpy as np
import matplotlib.pyplot as plt

R = Union[Number, np.ndarray, List[Number], Tuple[Number]]


def _is_num(val):
    return isinstance(val, Number)


def _len(val):
    if _is_num(val):
        return 1
    return len(val)


def _val(v):
    if isinstance(v, Sampler):
        return v()
    else:
        return v


class Sampler:
    def __init__(self, v=None):
        self.v = v

    def __call__(self):
        if self.v is None:
            return self.call()
        return self.v() if isinstance(self.v, Callable) else self.v

    def call(self):
        raise NotImplementedError

    def __mul__(self, other):
        return Sampler(lambda: self() * _val(other))

    def __rmul__(self, other):
        return Sampler(lambda: _val(other) * self())

    def __truediv__(self, other):
        return Sampler(lambda: self() / _val(other))

    def __rtruediv__(self, other):
        return Sampler(lambda: _val(other) / self())

    def __add__(self, other):
        return Sampler(lambda: self() + _val(other))

    def __radd__(self, other):
        return Sampler(lambda: _val(other) + self())

    def __sub__(self, other):
        return Sampler(lambda: self() - _val(other))

    def __rsub__(self, other):
        return Sampler(lambda: _val(other) - self())


class UniformFloatSampler(Sampler):
    def __init__(self, mi: R = 0., ma: R = 1.):
        super().__init__()
        self.single_val = _is_num(mi) and _is_num(ma)
        self.n = max(map(_len, [mi, ma]))
        self.mi, self.ma = np.array(mi), np.array(ma)

    def call(self):
        v = self.mi + np.random.rand(self.n) * (self.ma - self.mi)
        if self.single_val:
            return v[0]
        return v


class NormalFloatSampler(Sampler):
    def __init__(self, mu: R = 0., std: R = 1.,
                 mi: R = None, ma: R = None, round=False):
        super().__init__()
        mi = mi if mi is not None else -math.inf
        ma = ma if ma is not None else math.inf
        inputs = [mu, std, mi, ma]
        self.single_val = np.all([_is_num(v) for v in inputs])
        self.n = max(map(_len, inputs))
        self.mu, self.std = mu, std
        self.mi, self.ma = mi, ma
        self.round = round

    def call(self):
        values = np.empty(self.n, dtype=float)
        mask = np.ones(self.n, dtype=bool)
        while True:
            values[mask] = (np.random.randn(self.n) * self.std + self.mu)[mask]
            mask = np.logical_or(values < self.mi, values > self.ma)
            if mask.sum() == 0:
                break
        if self.round:
            values = np.round(values).astype(int)
        if self.single_val:
            return values[0]
        return values


class UniformIntSampler(Sampler):
    def __init__(self, mi: int, ma: int):
        super().__init__()
        self.mi, self.ma = mi, ma

    def call(self):
        return random.randint(self.mi, self.ma)


V = Union[Number, Sampler]


def unit_sphere_surface_sample():
    p = np.random.randn(3)
    return p / np.linalg.norm(p)


def unit_sphere_volume_sample():
    p = unit_sphere_surface_sample()
    p = p * r() ** (1 / 3)
    return p


def unit_shell_volume_sample(r_min: float):
    #assert 0 <= r_min <= 1
    p = unit_sphere_surface_sample()
    u_min = r_min ** 3
    u = u_min + r() * (1 - u_min)
    p = p * u ** (1 / 3)
    return p

def gen_rand_num(r_min, r_max):
    import random
    number = random.uniform(r_min, r_max)
    numbers = np.array([0, 0, number])
    return numbers

class UniformSphereVolumeSampler(Sampler):
    def __init__(self, radius=1., center=(0., 0., 0.), r_min=0., r_max=1.):
        super().__init__()
        self.center, self.radius, self.r_min, self.r_max = center, radius, r_min, r_max

    def call(self):
        randNum = gen_rand_num(self.r_min, self.r_max)
        return self.center + randNum


class UniformSphereSurfaceSampler(Sampler):
    def __init__(self, radius=1., center=(0., 0., 0.)):
        super().__init__()
        self.center, self.radius = center, radius

    def call(self):
        return self.center + unit_sphere_surface_sample() * self.radius

class UniformShellSampler(Sampler):
    def __init__(self, center=(0., 0., 0.), r_min=0., r_max=1.):
        super().__init__()
        assert r_min <= r_max
        self.center, self.r_min, self.r_max = center, r_min, r_max
        

    def call(self):
        return self.center + unit_shell_volume_sample(self.r_min / self.r_max) * self.r_max


class UniformQuaternionSampler(Sampler):
    def call(self):
        q = np.random.randn(4)
        return q / np.linalg.norm(q)


def plot_sampler_histogram(sampler, n=1000):
    values = [sampler() for _ in range(n)]
    plt.hist(values)
    plt.show()


def main():
    normal_float_sampler = 10 + NormalFloatSampler(mu=0, std=50, mi=5, ma=512)
    plot_sampler_histogram(normal_float_sampler)


if __name__ == '__main__':
    main()
