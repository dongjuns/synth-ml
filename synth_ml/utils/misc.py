import numpy as np
from progress.bar import Bar


def lazy_property(fn):
    # https://stevenloria.com/lazy-properties/
    attr_name = '_lazy_' + fn.__name__

    @property
    def _lazy_property(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)

    return _lazy_property


def np_immutable(a):
    im_a = np.array(a)
    im_a.setflags(write=False)
    return im_a


class ProgressBar(Bar):
    suffix = "%(index)d/%(max)d - %(percent).1f%% - %(eta_td)s"
