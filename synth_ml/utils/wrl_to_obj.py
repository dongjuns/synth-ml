import time
import numpy as np


def parse_wrl(filepath):
    IDLE, POINT, TRI = "IDLE", "POINT", "TRI"
    objs = []
    points = None
    tris = None
    state = IDLE
    for line in open(filepath).readlines():
        line = line.strip()
        if line.startswith("Group"):
            points = []
            tris = []
            obj = (points, tris)
            objs.append(obj)
        elif line == "]":
            state = IDLE
        elif state == IDLE:
            if line.startswith("point"):
                state = POINT
            elif line.startswith("coordIndex"):
                state = TRI
        elif state == POINT:
            point = [float(val) for val in line[:-1].split(" ")]
            assert len(point) == 3
            points.append(point)
        elif state == TRI:
            tri = [int(idx) for idx in line.split(",")[:4]]
            assert len(tri) == 4
            assert tri[3] == -1
            tri = tri[:3]
            tris.append(tri)
    for points, tris in objs:
        assert np.min(tris) == 0, np.max(tris) == len(points) - 1
    return objs


def write_obj(objects, filepath):
    with open(filepath, 'w') as f:
        f.write("# something\n# something\n")
        v_offset = 1
        for i, (points, tris) in enumerate(objects):
            f.write("o {}\n".format(i))
            for point in points:
                f.write("v {} {} {}\n".format(*point))
            f.write("s off\n")
            for tri in tris:
                tri = [v + v_offset for v in tri]
                f.write("f {} {} {}\n".format(*tri))
            v_offset += len(points)


def main():
    start = time.time()
    objects = parse_wrl("../objs/untitled.wrl")
    print("parse", time.time() - start)

    start = time.time()
    write_obj(objects, "../objs/vhacd.obj")
    print("write", time.time() - start)


if __name__ == '__main__':
    main()
