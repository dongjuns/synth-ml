from functools import partial


class Callback:
    def on_init(self):
        pass

    def on_next_frame(self):
        pass

    def on_render(self):
        pass

    def on_after_render(self):
        pass


class CallbackCompose(Callback):
    def __init__(self, *callbacks: Callback):
        self.callbacks = callbacks
        cb_names = [a for a in dir(Callback) if a.startswith("on_")]
        for cb_name in cb_names:
            setattr(self, cb_name, partial(self.call_cbs, cb_name))

    def call_cbs(self, cb_name):
        for cb in self.callbacks:
            getattr(cb, cb_name)()


def _main():
    class MyCallback(Callback):
        def __init__(self, name):
            self.name = name

        def on_init(self):
            print("init", self.name)

        def on_render(self):
            print("render", self.name)

    cbs = CallbackCompose(
        MyCallback("a"),
        CallbackCompose(
            MyCallback("b"),
            MyCallback("c"),
        )
    )

    cbs.on_init()
    cbs.on_next_frame()
    cbs.on_render()


if __name__ == '__main__':
    _main()
