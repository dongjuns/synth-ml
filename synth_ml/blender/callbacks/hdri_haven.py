"""
Thanks to https://hdrihaven.com/ for providing free hdris
Most of the hdris are in HDR format. The EXR files are ignored.
"""
# standard libs
import random
from pathlib import Path
from typing import List
import json
from math import pi
from multiprocessing.pool import ThreadPool
from functools import partial
import warnings
# dep libs
from pyquery import PyQuery as pq
from urllib.request import Request, urlopen
from urllib.error import HTTPError
# local libs
from ..callback import Callback
from ..wrappers import World, Scene, shader_nodes as sn
from ...utils.download import get_downloads_dir
from ...utils.misc import ProgressBar

categories = ['all', 'outdoor', 'skies', 'indoor', 'studio', 'nature', 'urban', 'night',
              'sunrise-sunset', 'morning-afternoon', 'midday', 'clear', 'partly-cloudy',
              'overcast', 'high-contrast', 'medium-contrast', 'low-contrast', 'natural-light',
              'artificial-light']
resolutions = [1, 2, 4, 8, 16]  # k


def _download_hdri_names(category: str):
    query = pq(url='https://hdrihaven.com/hdris/category/?c={}'.format(category.replace('-', ' ')))
    query = query('#hdri-grid').find('a')
    hdri_names = [a.attrib['href'].split('=')[-1] for a in query]
    return set(hdri_names)


def _get_hdri_format(hdri_name):
    query = pq(url='https://hdrihaven.com/hdri/?h={}'.format(hdri_name))
    return 'exr' if 'EXR' in query.html() else 'hdr'


def _download_hdri(folder, filename: str):
    url = 'https://hdrihaven.com/files/hdris/{}'.format(filename)
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    try:
        res = urlopen(req)
    except HTTPError as e:
        return False
    data = res.read()
    with open(folder / filename, 'wb') as f:
        f.write(data)
    return True


def _get_hdri_haven_folder():
    folder = get_downloads_dir() / 'hdri_haven'
    if not folder.exists():
        folder.mkdir()
    return folder


def _get_hdri_haven_meta_data():
    meta_data_file = Path(__file__).parent / 'hdri_haven_meta_data.json'
    if not meta_data_file.exists():
        meta_data = {}
        for category in Bar('Getting hdri category lists..').iter(categories):
            meta_data[category] = _download_hdri_names(category)
        hdri_names = set(meta_data['all'])
        for name in Bar('Getting hdri format..').iter(set(hdri_names)):
            if not _get_hdri_format(name) == 'hdr':
                hdri_names.remove(name)
        for category in categories:
            meta_data[category] = list(meta_data[category] & hdri_names)
        json.dump(meta_data, meta_data_file.open('w'), indent=4)
    return json.load(meta_data_file.open('r'))


def _download_files(hdri_names: List[str], resolution: int):
    folder = _get_hdri_haven_folder() / 'hdris'
    if not folder.exists():
        folder.mkdir()
    file_paths = []
    should_download = []
    for name in hdri_names:
        filename = '{}_{}k.hdr'.format(name, resolution)
        file_path = folder / filename
        if file_path.exists():
            file_paths.append(file_path)
        else:
            should_download.append(filename)
    if should_download:
        pool = ThreadPool(4)
        bar = ProgressBar("Downloading hdr files..", max=len(should_download))
        for filename, success in zip(should_download, pool.imap(partial(_download_hdri, folder), should_download)):
            bar.next()
            if success:
                file_paths.append(folder / filename)
        bar.finish()
    if len(hdri_names) > len(file_paths):
        print("Warning: only {} of {} hdris were downloaded".format(len(file_paths), len(hdri_names)))

    return file_paths


class HdriEnvironment(Callback):
    def __init__(self, scene: Scene, category: str = 'all', resolution: int = 1, max_altitude_deg: float = 20):
        assert category in categories
        assert resolution in resolutions
        import bpy
        self.bpy = bpy
        self.scene = scene
        self.category = category
        self.resolution = resolution
        self.max_altitude_deg = max_altitude_deg
        self.hdri_names = _get_hdri_haven_meta_data()[category]
        self.hdri_paths = _download_files(self.hdri_names, resolution)

    def on_init(self):
        self.hdris = [self.bpy.data.images.load(str(p.absolute())) for p in self.hdri_paths]

        world = World()
        self.scene.world = world

        # shader_nodes nodes
        tex_coord = sn.TextureCoordinate(world)
        self.local_z = sn.Mapping(world, type=sn.Mapping.VECTOR)
        self.global_y = sn.Mapping(world, type=sn.Mapping.VECTOR)
        self.global_z = sn.Mapping(world, type=sn.Mapping.VECTOR)
        self.env_tex = sn.EnvironmentTexture(world)
        background = sn.Background(world)
        world_output = sn.WorldOutput(world)

        # links
        tex_coord.out_generated() >> self.local_z.in_vector()
        self.local_z.out_vector() >> self.global_y.in_vector()
        self.global_y.out_vector() >> self.global_z.in_vector()
        self.global_z.out_vector() >> self.env_tex.in_vector()
        self.env_tex.out_color() >> background.in_color()
        background.out_background() >> world_output.in_surface()


    def on_next_frame(self):
        r = random.random
        self.env_tex.image = random.choice(self.hdris)
        self.local_z.in_rotation().default_value[2] = r() * pi * 2  # 0-360 deg azimuth
        self.global_z.in_rotation().default_value[2] = r() * pi * 2  # 0-360 deg azimuth
        self.global_y.in_rotation().default_value[1] = r() * self.max_altitude_deg * pi / 180
