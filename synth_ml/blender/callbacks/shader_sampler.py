import numbers

import numpy as np
from scipy.spatial.transform import Rotation

from ...utils.rand import Sampler, NormalFloatSampler, UniformFloatSampler
from ..wrappers import shader_nodes as sn, NodeInput
from ...utils.rand import UniformQuaternionSampler
from ..callback import Callback


def callback_from_sampler(node_input: NodeInput, sampler: Sampler):
    class _Callback(Callback):
        def on_next_frame(self):
            node_input.link(sampler())

    return _Callback()


class BoolSampler(Callback):
    def __init__(self, node_input: NodeInput, p=0.5, to_int=False):
        self.inp = node_input
        self.p = p
        self.to_int = to_int

    def on_next_frame(self):
        v = np.random.rand() < self.p
        self.inp.link(int(v) if self.to_int else v)


class FacSampler(Callback):
    def __init__(self, node_input: NodeInput, sampler=UniformFloatSampler()):
        self.inp = node_input
        self.sampler = Sampler(sampler)

    def on_next_frame(self):
        self.inp.link(self.sampler())


class ColorSampler(Callback):
    def __init__(self, node_input: NodeInput, sampler=UniformFloatSampler((0, 0, 0, 1), (1, 1, 1, 1))):
        self.inp = node_input
        self.sampler = sampler

    def on_next_frame(self):
        self.inp.link(self.sampler())

class VoronoiSampler(Callback):
    def __init__(
            self, voronoi: sn.VoronoiTexture,
            scale=1 / UniformFloatSampler(1 / 30, 1 / 0.5),
            randomness=UniformFloatSampler(),
    ):
        self.voronoi = voronoi
        self.scale = Sampler(scale)
        self.randomness = Sampler(randomness)

    def on_next_frame(self):
        self.voronoi.in_scale(self.scale())
        self.voronoi.in_randomness(self.randomness())


class NoiseSampler(Callback):
    def __init__(
            self, noise: sn.NoiseTexture,
            scale=1 / UniformFloatSampler(1 / 30, 1 / 0.5),
            detail=UniformFloatSampler(0, 3),
            distortion=UniformFloatSampler(0, 2),
    ):
        self.noise = noise
        self.scale = Sampler(scale)
        self.detail = Sampler(detail)
        self.distortion = Sampler(distortion)

    def on_next_frame(self):
        self.noise.in_scale(self.scale())
        self.noise.in_detail(self.detail())
        self.noise.in_distortion(self.distortion())


class MappingRotationSampler(Callback):
    def __init__(self, mapping: sn.Mapping, scale=1.):
        if isinstance(scale, numbers.Number):
            scale = [scale] * 3
        self.mapping = mapping
        self.mapping.in_scale(scale)
        self.quaternion_sampler = UniformQuaternionSampler()
        pass

    def on_next_frame(self):
        q = self.quaternion_sampler()
        r = Rotation(q).as_euler('XYZ')
        self.mapping.in_rotation(r)
        self.mapping.in_location(np.random.rand(3) * 100)
