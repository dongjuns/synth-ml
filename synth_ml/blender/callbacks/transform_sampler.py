import math
import numpy as np

from ..callback import Callback
from ...utils import rand


def _get_axis_idx(axis):
    if type(axis) == str:
        axis = 'xyz'.index(axis.lower())
    assert type(axis) == int
    return axis


class TransformSampler(Callback):
    def __init__(self):
        self._samplers = []

    def on_next_frame(self):
        for f, args in self._samplers:
            f(*args)

    def add(self, f, *args):
        self._samplers.append((f, args))

    def add_pos_sampler(self, obj, sampler):
        def f():
            obj().location = sampler()

        self.add(f)

    def add_pos_axis_sampler(self, obj, axis, sampler):
        axis = _get_axis_idx(axis)

        def f():
            obj().location[axis] = sampler()

        self.add(f)

        
    def add_rot_axis_sampler(self, obj, axis, sampler):
        if type(axis) == str:
            axis = 'xyz'.index(axis.lower())
        assert type(axis) == int

        def f():
            obj().rotation_euler[axis] = sampler()

        self.add(f)


    def pos_sphere_volume(self, obj, radius: float, center=None):
        if center is None:
            center = list(obj().location)
        sampler = rand.UniformSphereVolumeSampler(center=center, radius=radius, r_min=r_min, r_max=r_max)
        self.add_pos_sampler(obj, sampler)


    def pos_sphere_surface_uniform(self, obj, radius: float, center=None):
        if center is None:
            center = list(obj().location)
        sampler = rand.UniformSphereSurfaceSampler(center, radius)
        self.add_pos_sampler(obj, sampler)

    def pos_sphere_shell(self, obj, center, r_min, r_max):
        sampler = rand.UniformShellSampler(center, r_min, r_max)
        self.add_pos_sampler(obj, sampler)

    def pos_axis_uniform(self, obj, axis, mi: float, ma: float):
        self.add_pos_axis_sampler(obj, axis, rand.UniformFloatSampler(mi, ma))
        
        
    def rot_axis_uniform(self, obj, axis, mi=0, ma=2 * math.pi):
        if type(axis) == str:
            axis = 'xyz'.index(axis.lower())
        assert type(axis) == int
        sampler = rand.UniformFloatSampler(mi, ma)

        def f():
            obj().rotation_euler[axis] = sampler()

        self.add(f)
