from pathlib import Path
from typing import Union, List, Tuple, Dict
import json

import transformations as t
import numpy as np

from ..callback import Callback
from ...utils.misc import lazy_property, np_immutable
from ..wrappers import Object, Scene
from ..bl_utils import get_default_output_folder


class MetadataLogger(Callback):
    """metadata callback class"""

    def __init__(self, scene: Scene, output_folder: Union[Path, str] = None,
                 objects: List[Object] = None):
        super().__init__()
        if output_folder is None:
            output_folder = get_default_output_folder()
        self.scene = scene
        self.folder = Path(output_folder) / "metadata"
        self.folder.mkdir(exist_ok=True)
        self.objects = objects or []

    def on_after_render(self):
        cam = self.scene.camera
        focal_length = cam().data.lens
        sensor_size = cam().data.sensor_width
        cam_t_world = [list(v) for v in cam.matrix_world]

        metadata = {
            "frame": self.scene.frame,
            "camera": {
                "resolution": self.scene.resolution,
                "t_world": cam_t_world,
                "focal_length": focal_length,
                "sensor_width": sensor_size,  # normally horizontal sensor fit
            },
            "objects": {},
        }

        for obj in self.objects:
            metadata["objects"][obj.name] = {
                "t_world": [list(v) for v in obj.matrix_world],
                # material ?, etc
            }

        file_name = self.folder / "{:04}.json".format(self.scene.frame)
        json.dump(metadata, open(file_name, 'w'), indent=4)


class ObjectMetaData:
    def __init__(self, name, data: dict):
        self.name = name
        self.data = data

    @lazy_property
    def t_world(self):
        return np_immutable(self.data["t_world"])

    @property
    def position(self):
        return self.t_world[:3, 3]

    @property
    def R(self):
        return self.t_world[:3, :3]


class Metadata:
    """meta data loader class"""

    def __init__(self, fp: Union[Path, str]):
        self.fp = Path(fp)

    @lazy_property
    def data(self):
        return json.load(self.fp.open())

    @lazy_property
    def frame(self):
        return self.data["frame"]

    @lazy_property
    def resolution(self):
        return np_immutable(self.data["camera"]["resolution"])

    @lazy_property
    def cam_t_world(self):
        return np_immutable(self.data["camera"]["t_world"])

    @lazy_property
    def focal_length(self):
        return self.data["camera"]["focal_length"]

    @lazy_property
    def sensor_width(self):
        return self.data["camera"]["sensor_width"]

    @lazy_property
    def alpha(self):
        return self.focal_length * max(self.resolution) / self.sensor_width

    @lazy_property
    def world_t_cam(self):
        return np_immutable(t.inverse_matrix(self.cam_t_world))

    @lazy_property
    def world_t_image(self):
        a = self.alpha
        w, h = self.resolution
        cam_t_image = np.array([
            [a, 0, -w / 2, 0],
            [0, -a, -h / 2, 0],
            [0, 0, -1, 0],
            [0, 0, 0, 1]
        ])
        return np_immutable(cam_t_image @ self.world_t_cam)

    @lazy_property
    def objects(self):
        return {name: ObjectMetaData(name, data) for name, data in self.data["objects"].items()}

    def world_2_image(self, p_world):
        p_world = np.array(p_world)
        if len(p_world.shape) == 1:
            p_world = p_world.reshape(-1, 1)
        if p_world.shape[0] == 3:
            p_world = np.concatenate((p_world, np.ones((1, p_world.shape[1]))))
        assert p_world.shape[0] == 4
        p_image = self.world_t_image @ p_world
        xy = p_image[:2] / p_image[2]
        z = p_image[2:3] / p_image[3:4]
        return np.concatenate((xy, z))

    def bbox(self, obj_verts: np.ndarray, obj_t_world: np.ndarray, round=True, clip=True):
        D, N = obj_verts.shape
        if D == 3:
            obj_verts = np.concatenate((obj_verts, np.ones((1, N))))
        assert obj_verts.shape[0] == 4, "obj_verts must be in homogeneous coordinates w. shape: (4, N)"
        obj_t_image = self.world_t_image @ obj_t_world
        verts_image = obj_t_image @ obj_verts
        verts_image = verts_image[:2] / verts_image[2:3]  # project to image
        # find bbox
        xmin, xmax = verts_image[0].min(), verts_image[0].max()
        ymin, ymax = verts_image[1].min(), verts_image[1].max()
        bbox = np.array((xmin, ymin, xmax, ymax))
        if clip:
            bbox[[0, 2]] = np.clip(bbox[[0, 2]], 0, self.resolution[0] - 1)
            bbox[[1, 3]] = np.clip(bbox[[1, 3]], 0, self.resolution[1] - 1)
        if round:
            bbox = np.round(bbox).astype(int)
        return bbox
