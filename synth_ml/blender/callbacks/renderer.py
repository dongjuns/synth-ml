from typing import Union

from pathlib import Path

from ...utils import rand as rand
from ..bl_utils import get_default_output_folder, CYCLES, EEVEE, RENDER_ENGINES
from ..wrappers import Scene, compositor_nodes as cn
from ..callback import Callback


class Renderer(Callback):
    image_file_output = None
    depth_file_output = None
    denoise_file_output = None
    normal_file_output = None

    def __init__(self, scene: Scene, resolution, engines,
                 output_folder: Union[Path, str] = None, exist_ok=True,
                 cycles_samples: rand.V = 128, eevee_samples: rand.V = 64,
                 save_image=True, alpha=False,
                 depth=False, denoise=False, normal=False, taskName="", dateTime="", directoryName=""):
        import bpy
        self.bpy = bpy
        self.scene = scene


        self.resolution_sampler = rand.Sampler(resolution)
        self.cycles_samples_sampler = rand.Sampler(cycles_samples)
        self.eevee_samples_sampler = rand.Sampler(eevee_samples)
        scene.film_transparent = alpha
        if type(engines) == str:
            engines = (engines,)
        assert set(engines) <= set(RENDER_ENGINES), "engines not supported: {}".format(
            set(engines) - set(RENDER_ENGINES))
        self.engines = engines

        self.base_folder = Path(output_folder if output_folder else get_default_output_folder(taskName, dateTime, directoryName))
        self.base_folder.mkdir(exist_ok=exist_ok)

        self.scene.use_nodes = True
        self.scene.node_tree.clear()

        self.render_layers = cn.RenderLayers(scene)
        self.composite = cn.Composite(scene)

        if save_image:
            self.image_file_output = cn.FileOutput(
                self.scene, folder=self.base_folder, file_format=cn.FileOutput.PNG,
                color_mode='RGBA' if alpha else 'RGB', image=self.render_layers.out_image()
            )

        if depth:
            # saved depth: 1 / (Z + 1), as 16bit PNG
            scene.set_render_pass('z', True)
            self.depth_add = cn.Math(self.scene, operation=cn.Math.ADD,
                                     value_1=self.render_layers.out_depth(), value_2=1.)
            self.depth_div = cn.Math(self.scene, operation=cn.Math.DIVIDE,
                                     value_1=1., value_2=self.depth_add.out_value())
            self.depth_file_output = cn.FileOutput(
                self.scene, folder=self.base_folder / "depth", file_format=cn.FileOutput.PNG,
                color_mode='BW', color_depth=cn.FileOutput.sixteen, image=self.depth_div.out_value()
            )

        if denoise:
            assert CYCLES in engines, "denoise only applicable to the CYCLES engine"
            scene.set_render_pass('diffuse_color', True)
            scene.set_render_pass('normal', True)
            self.denoise = cn.Denoise(
                scene,
                image=self.render_layers.out_image(),
                normal=self.render_layers.out_normal(),
                albedo=self.render_layers.out_diff_col(),
            )
            self.denoise_file_output = cn.FileOutput(
                self.scene, folder=self.base_folder / "cycles_denoise", file_format=cn.FileOutput.PNG,
                color_mode='RGBA' if alpha else 'RGB', image=self.denoise.out_image()
            )

        if normal:
            scene.set_render_pass('normal', True)
            add = cn.Mix(scene, blend_type=cn.Mix.ADD,
                         image_1=self.render_layers.out_normal(), image_2=(1, 1, 1, 1))
            divide = cn.Mix(scene, blend_type=cn.Mix.DIVIDE,
                            image_1=add.out_image(), image_2=(2, 2, 2, 1))
            self.normal_file_output = cn.FileOutput(
                self.scene, folder=self.base_folder / "normal_world", file_format=cn.FileOutput.PNG,
                color_mode='RGB', color_depth=cn.FileOutput.sixteen, image=divide.out_image()
            )

    def on_next_frame(self):
        self.scene.resolution = self.resolution_sampler()
        self.scene.set_render_samples(CYCLES, self.cycles_samples_sampler())
        self.scene.set_render_samples(EEVEE, self.eevee_samples_sampler())

    def on_render(self):
        for i, engine in enumerate(self.engines):
            first_render = i == 0
            if self.depth_file_output:
                self.depth_file_output.mute = not first_render
            if self.normal_file_output:
                self.normal_file_output.mute = not first_render
            if self.denoise_file_output:
                mute_denoise = engine != CYCLES
                self.denoise.mute = mute_denoise
                self.denoise_file_output.mute = mute_denoise
            self.image_file_output.base_path = self.base_folder / engine.split("_")[-1].lower()
            self.scene.set_render_engine(engine)
            self.bpy.ops.render.render()
