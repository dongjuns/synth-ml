import numpy as np

from ..wrappers import Material, shader_nodes as sn
from ..callback import CallbackCompose
from ..callbacks import shader_sampler as ss
from ...utils.rand import UniformFloatSampler


class UniformMaterial(Material):
    def __init__(self):
        super().__init__()
        # nodes
        self.bevel = sn.Bevel(self)
        self.bsdf = sn.PrincipledBSDF(self)
        self.out = sn.MaterialOutput(self)

        # links
        self.bevel.out_normal() >> self.bsdf.in_normal()
        self.bsdf.out_bsdf() >> self.out.in_surface()

    def sampler_cb(self, p_metallic=0.5,
                   base_color=UniformFloatSampler((0, 0, 0, 1), (1, 1, 1, 1)),
                   roughness=UniformFloatSampler(),
                   bevel_radius=UniformFloatSampler(0, 0.001)):
        return CallbackCompose(
            ss.ColorSampler(self.bsdf.in_base_color(), base_color),
            ss.BoolSampler(self.bsdf.in_metallic(), p=p_metallic, to_int=True),
            ss.FacSampler(self.bsdf.in_roughness(), roughness),
            ss.FacSampler(self.bevel.in_radius(), bevel_radius),
        )
