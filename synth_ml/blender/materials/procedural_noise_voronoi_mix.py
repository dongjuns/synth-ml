from ..wrappers import Material, shader_nodes as sn
from ..callback import CallbackCompose
from ..callbacks import shader_sampler as ss
from ...utils.rand import UniformFloatSampler


class ProceduralNoiseVoronoiMix(Material):
    def __init__(self):
        super().__init__()
        # nodes
        self.tex_coord = sn.TextureCoordinate(self)

        self.mapping = sn.Mapping(self, type=sn.Mapping.VECTOR)

        self.noise = sn.NoiseTexture(self)
        self.voronoi = sn.VoronoiTexture(self)

        self.mix = sn.MixRGB(self, blend_type=sn.MixRGB.OVERLAY, fac=.5)

        self.bevel = sn.Bevel(self)

        self.bsdf = sn.PrincipledBSDF(self)
        self.out = sn.MaterialOutput(self)

        # links

        self.tex_coord.out_camera() >> self.mapping.in_vector()

        self.mapping.out_vector() >> self.noise.in_vector()
        self.mapping.out_vector() >> self.voronoi.in_vector()

        self.noise.out_color() >> self.mix.in_color_1()
        self.voronoi.out_color() >> self.mix.in_color_2()

        self.mix.out_color() >> self.bsdf.in_base_color()

        self.bevel.out_normal() >> self.bsdf.in_normal()

        self.bsdf.out_bsdf() >> self.out.in_surface()

    def sampler_cb(self, scale=1., p_metallic=0.5,
                   roughness=UniformFloatSampler(),
                   bevel_radius=UniformFloatSampler(0, 0.001)):
        return CallbackCompose(
            ss.MappingRotationSampler(self.mapping, scale=scale),
            ss.VoronoiSampler(self.voronoi),
            ss.NoiseSampler(self.noise),
            ss.BoolSampler(self.bsdf.in_metallic(), p=p_metallic, to_int=True),
            ss.FacSampler(self.bsdf.in_roughness(), roughness),
            ss.FacSampler(self.bevel.in_radius(), bevel_radius),
        )
