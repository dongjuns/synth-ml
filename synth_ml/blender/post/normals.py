import numpy as np


def world_to_cam(normals_world: np.ndarray, world_T_cam: np.ndarray):
    input_shape = normals_world.shape
    normals_cam = None
