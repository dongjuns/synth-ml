from .object import Object

class Camera(Object):
    def __init__(self, name="camera", scene=None, parent=None, location=None, blender_obj=None,
                 fov: float = None, focal_length: float = None, clip_start: float = None, clip_end: float = None, sensor_width: float = None):
        assert not (fov and focal_length), "specify either fov or focal length, not both"
        import bpy
        
        super_kwargs = dict(name=name, scene=scene, parent=parent, location=location)
        if blender_obj:
            super_kwargs["blender_obj"] = blender_obj
        else:
            super_kwargs["object_data"] = bpy.data.cameras.new(name)
        super().__init__(**super_kwargs)

        if scene and scene.camera is None:
            scene.camera = self
        
        if sensor_width is not None:
            self.sensor_width = sensor_width
        if focal_length is not None:
            self.focal_length = focal_length
        if clip_start is not None:
            self.clip_start = clip_start
        if clip_end is not None:
            self.clip_end = clip_end


    @classmethod
    def from_blender_obj(cls, blender_obj):
        return

    @property
    def clip_start(self):
        return self().data.clip_start

    @clip_start.setter
    def clip_start(self, clip_start: float):
        self().data.clip_start = clip_start

    @property
    def clip_end(self):
        return self().data.clip_end

    @clip_end.setter
    def clip_end(self, clip_end: float):
        self().data.clip_end = clip_end

    @property
    def focal_length(self):
        return self().data.lens

    @focal_length.setter
    def focal_length(self, focal_length: float):
        self().data.lens = focal_length

    @property
    def fov(self):
        return self().data.angle

    @fov.setter
    def fov(self, fov: float):
        self().data.angle = fov

    @property
    def sensor_width(self):
        return self().data.sensor_width

    @sensor_width.setter
    def sensor_width(self, sensor_width: float):
        self().data.sensor_width = sensor_width
