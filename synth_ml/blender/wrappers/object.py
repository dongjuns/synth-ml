from . import constraints as cnstr
from ..bl_utils.redirect import BlenderRedirect


class Object:
    def __init__(self, name="object", object_data=None, blender_obj=None, scene=None, parent=None, location=None):
        import bpy
        self.bpy = bpy
        if blender_obj is None:
            blender_obj = bpy.data.objects.new(name, object_data)
        self.name = blender_obj.name
        self._look_at_constraint = None
        if scene:
            scene.add(self)
        if parent:
            self.parent = parent
        if location:
            self.location = location

    def __call__(self):
        return self.bpy.data.objects[self.name]

    @staticmethod
    def get_blender_obj(obj):
        return obj() if isinstance(obj, Object) else obj

    @classmethod
    def from_name(cls, obj_name):
        import bpy
        return cls(blender_obj=bpy.data.objects[obj_name])

    @classmethod
    def import_obj(cls, obj_path, scene=None):
        import bpy
        # bpy ops import automatically links the object to the active collection,
        # but we don't generally want that.
        # It also doesn't return references to the loaded objects.
        obj_names_before = set(bpy.context.collection.objects.keys())
        with BlenderRedirect():
            bpy.ops.import_scene.obj(filepath=str(obj_path))
        obj_names_after = set(bpy.context.collection.objects.keys())
        new_obj_names = obj_names_after - obj_names_before
        new_objs = [cls(blender_obj=bpy.data.objects[n]) for n in new_obj_names]
        for o in new_objs:
            bpy.context.collection.objects.unlink(o())
        if scene:
            for obj in new_objs:
                scene.add(obj)
        return new_objs

    def look_at(self, obj=None, track_axis=cnstr.TRACK_NEGATIVE_Z, up_axis=cnstr.UP_Y):
        if self._look_at_constraint is not None:
            self().constraints.remove(self._look_at_constraint)
        if obj is not None:
            if type(obj) in (list, tuple):
                assert len(obj) == 3  # pos
                obj, pos = Object("look_at target"), obj
                obj.location = pos
            self._look_at_constraint = self().constraints.new(type=cnstr.TRACK_TO)
            self._look_at_constraint.target = self.get_blender_obj(obj)
            self._look_at_constraint.track_axis = track_axis
            self._look_at_constraint.up_axis = up_axis
        return self

    @property
    def location(self):
        return self().location

    @location.setter
    def location(self, loc):
        self().location = loc

    @property
    def parent(self):
        return self().parent

    @parent.setter
    def parent(self, parent):
        self().parent = self.get_blender_obj(parent)

    @property
    def children(self):
        return [Object(blender_obj=child) for child in self().children]

    @property
    def matrix_world(self):
        return self().matrix_world

    def assign_material(self, material):
        materials = self().data.materials
        for i in range(len(materials)):
            materials[i] = material()
