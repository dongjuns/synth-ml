import numbers
from typing import Iterable

from .object import Object
from .world import World
from .nodes import NodeTree
from ..bl_utils.misc import CYCLES, EEVEE, WORKBENCH
from ..callback import Callback
from ...utils.misc import ProgressBar


class Scene:
    def __init__(self, name="scene", callback: Callback = Callback(), remove_other_scenes=True, blender_scene=None):
        import bpy
        self.bpy = bpy
        scene = bpy.data.scenes.new(name) if blender_scene is None else blender_scene
        self.name = scene.name
        if remove_other_scenes:
            for name in bpy.data.scenes.keys():
                if not name == self.name:
                    bpy.data.scenes.remove(bpy.data.scenes[name])
        self.callback = callback
        scene.cycles.device = "GPU"  # will use cpu if no devices are enabled
        self.film_transparent = True
        self.frame = -1
        self.initiated = False

    def __call__(self):
        return self.bpy.data.scenes[self.name]

    def init(self):
        if not self.initiated:
            self.callback.on_init()
            self.initiated = True

    def next_frame(self, frame=None):
        self.init()
        self.frame = self.frame + 1 if frame is None else frame
        self.callback.on_next_frame()

    def render(self):
        self.init()
        self.callback.on_render()
        self.callback.on_after_render()

    def render_frames(self, frames: Iterable):
        self.init()
        for frame in ProgressBar("Rendering..").iter(frames):
            self.next_frame(frame)
            self.render()

    def _add_blender_obj(self, obj):
        self().collection.objects.link(obj)
        for child in obj.children:
            self._add_blender_obj(child)

    def add(self, obj):
        self._add_blender_obj(obj())

    def set_render_engine(self, engine):
        assert engine in (CYCLES, EEVEE, WORKBENCH)
        self().render.engine = engine

    def set_render_samples(self, engine: str, samples: int):
        if engine == CYCLES:
            self().cycles.samples = samples
        elif engine == EEVEE:
            self().eevee.taa_render_samples = samples
        else:
            raise TypeError("Setting samples on engine '{}' is not supported".format(engine))

    # Todo: return camera instance
    @property
    def camera(self):
        return Object(blender_obj=self().camera) if self().camera else None

    @camera.setter
    def camera(self, camera):
        self().camera = Object.get_blender_obj(camera)

    @property
    def use_nodes(self):
        return self().use_nodes

    @use_nodes.setter
    def use_nodes(self, use_nodes: bool):
        self().use_nodes = use_nodes

    @property
    def world(self):
        return self().world

    @world.setter
    def world(self, world):
        self().world = world() if isinstance(world, World) else world

    @property
    def node_tree(self):
        return NodeTree(self())

    @property
    def film_transparent(self):
        return self().render.film_transparent

    @film_transparent.setter
    def film_transparent(self, film_transparent: bool):
        self().render.film_transparent = film_transparent

    @property
    def resolution(self):
        return self().render.resolution_x, self().render.resolution_y

    @resolution.setter
    def resolution(self, x, y=None):
        if y is None:
            x, y = (x, x) if isinstance(x, numbers.Number) else x
        self().render.resolution_x = x
        self().render.resolution_y = y

    @property
    def tile_size(self):
        return self().render.tile_x, self().render.tile_y

    @tile_size.setter
    def tile_size(self, x, y=None):
        if y is None:
            x, y = (x, x) if isinstance(x, numbers.Number) else x
        self().render.tile_x = x
        self().render.tile_y = y

    def link(self, inp, out):
        self().node_tree.links.new(inp, out)

    def set_render_pass(self, pass_name, enabled=True):
        view_layer = self().view_layers["View Layer"]
        setattr(view_layer, 'use_pass_' + pass_name, enabled)

    @property
    def frame(self):
        return self().frame_current

    @frame.setter
    def frame(self, frame: int):
        self().frame_set(frame)

    @classmethod
    def from_name(cls, name: str):
        import bpy
        return cls(blender_scene=bpy.data.scenes[name])
