from typing import Union


class NodeInput:
    def __init__(self, node, key: str, link=None):
        self.node = node  # type: Node
        self.key = key
        if link is not None:
            self.link(link)

    def __call__(self):
        return self.node.b_input(self.key)

    def link(self, output):
        if isinstance(output, NodeOutput):
            self.node.node_tree.link(self(), output())
        else:
            self.default_value = output

    def __lshift__(self, other):
        self.link(other)

    @property
    def default_value(self):
        return self().default_value

    @default_value.setter
    def default_value(self, value):
        self().default_value = value


class NodeOutput:
    def __init__(self, node, key: str, link=None):
        self.node = node  # type: Node
        self.key = key
        if link is not None:
            self.link(link)

    def __call__(self):
        return self.node.b_output(self.key)

    def link(self, input: NodeInput):
        self.node.node_tree.link(input(), self())

    def __rshift__(self, other: NodeInput):
        self.link(other)


class NodeTree:
    def __init__(self, node_tree):
        success = False
        while hasattr(node_tree, "node_tree"):
            node_tree = node_tree.node_tree
            success = True
        assert success, "object has no attribute node_tree"
        self.node_tree = node_tree

    @property
    def nodes(self):
        return self.node_tree.nodes

    def new(self, node_type: str):
        return self.node_tree.nodes.new(node_type)

    def link(self, inp, out):
        return self.node_tree.links.new(inp, out)

    def clear(self):
        nodes = self.nodes
        for node in nodes.values():
            nodes.remove(node)


class Node:
    def __init__(self, node_tree, node_type: str):
        self.node_tree = NodeTree(node_tree)
        self.name = self.node_tree.new(node_type).name

    def __call__(self):
        return self.node_tree.nodes[self.name]

    @property
    def mute(self):
        return self().mute

    @mute.setter
    def mute(self, mute: bool):
        self().mute = mute

    def input(self, key, link=None):
        return NodeInput(self, key, link)

    def output(self, key, link=None):
        return NodeOutput(self, key, link)

    def b_input(self, key: Union[str, int]):
        return self().inputs[key]

    def b_output(self, key: Union[str, int]):
        return self().outputs[key]
