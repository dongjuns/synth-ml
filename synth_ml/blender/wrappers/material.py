from .nodes import NodeTree


class Material:
    def __init__(self):
        import bpy
        self.bpy = bpy
        self.name = bpy.data.materials.new("material").name
        self().use_nodes = True
        self.node_tree = NodeTree(self())
        self.node_tree.clear()

    def __call__(self):
        return self.bpy.data.materials[self.name]

    def get_nodes(self):
        return self().node_tree.nodes

    def get_node(self, name: str):
        return self.get_nodes()[name]

    def link(self, inp, out):
        self.node_tree.link(inp, out)
