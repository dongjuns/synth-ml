from .nodes import NodeTree


class World:
    def __init__(self):
        import bpy
        self.bpy = bpy
        self.name = bpy.data.worlds.new("world").name
        self().use_nodes = True
        self.node_tree = NodeTree(self())
        self.node_tree.clear()

    def __call__(self):
        return self.bpy.data.worlds[self.name]
