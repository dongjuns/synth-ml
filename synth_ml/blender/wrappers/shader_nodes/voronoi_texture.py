from .shader_node import ShaderNode


class VoronoiTexture(ShaderNode):
    # features
    F1 = "F1"
    F2 = "F2"
    SMOOTH_F1 = "SMOOTH_F1"
    DISTANCE_TO_EDGE = "DISTANCE_TO_EDGE"
    N_SPHERE_RADIUS = "N_SPHERE_RADIUS"

    # distance
    EUCLIDEAN = "EUCLIDEAN"
    MANHATTAN = "MANHATTAN"
    CHEBYCHEV = "CHEBYCHEV"
    MINKOWSKI = "MINKOWSKI"

    def __init__(self, node_tree):
        super().__init__(node_tree, "TexVoronoi")

    @property
    def dimensions(self):
        return int(self().voronoi_dimensions[0])

    @dimensions.setter
    def dimensions(self, d: int):
        assert 1 <= d <= 4
        self().voronoi_dimensions = "{}D".format(d)

    @property
    def feature(self):
        return self().feature

    @feature.setter
    def feature(self, feature):
        self().feature = feature

    @property
    def distance(self):
        return self().distance

    @distance.setter
    def distance(self, distance):
        self().distance = distance

    # in
    def in_exponent(self, link=None):
        return self.input("Exponent", link)

    def in_randomness(self, link=None):
        return self.input("Randomness", link)

    def in_scale(self, link=None):
        return self.input("Scale", link)

    def in_smoothness(self, link=None):
        return self.input("Smoothness", link)

    def in_vector(self, link=None):
        return self.input("Vector", link)

    def in_w(self, link=None):
        return self.input("W", link)

    # out
    def out_color(self, link=None):
        return self.output("Color", link)

    def out_distance(self, link=None):
        return self.output("Distance", link)

    def out_position(self, link=None):
        return self.output("Position", link)

    def out_radius(self, link=None):
        return self.output("Radius", link)

    def out_w(self, link=None):
        return self.output("W", link)
