from .shader_node import ShaderNode


class EnvironmentTexture(ShaderNode):
    # interpolation
    LINEAR = 'Linear'
    CLOSEST = 'Closest'
    CUBIC = 'Cubic'
    SMART = 'Smart'
    # projection
    EQUIRECTANGULAR = 'EQUIRECTANGULAR'
    MIRROR_BALL = 'MIRROR_BALL'

    def __init__(self, node_tree, interpolation: str = None, projection: str = None):
        super().__init__(node_tree, 'TexEnvironment')
        if interpolation:
            self.interpolation = interpolation
        if projection:
            self.projection = projection

    @property
    def image(self):
        return self().image

    @image.setter
    def image(self, image):
        self().image = image

    @property
    def interpolation(self):
        return self().interpolation

    @interpolation.setter
    def interpolation(self, interpolation: str):
        self().interpolation = interpolation

    @property
    def projection(self):
        return self().projection

    @projection.setter
    def projection(self, projection: str):
        self().projection = projection

    def in_vector(self, link=None):
        return self.input('Vector', link)

    def out_color(self, link=None):
        return self.output('Color', link)
