from .shader_node import ShaderNode


class PrincipledBSDF(ShaderNode):
    def __init__(self, node_tree):
        super().__init__(node_tree, "BsdfPrincipled")

    def out_bsdf(self, link=None):
        return self.output("BSDF", link)

    def in_alpha(self, link=None):
        return self.input("Alpha", link)

    def in_anisotropic(self, link=None):
        return self.input("Anisotropic", link)

    def in_anisotropic_rotation(self, link=None):
        return self.input("Anisotropic Rotation", link)

    def in_base_color(self, link=None):
        return self.input("Base Color", link)

    def in_clearcoat(self, link=None):
        return self.input("Clearcoat", link)

    def in_clearcoat_normal(self, link=None):
        return self.input("Clearcoat Normal", link)

    def in_clearcoat_roughness(self, link=None):
        return self.input("Clearcoat Roughness", link)

    def in_emission(self, link=None):
        return self.input("Emission", link)

    def in_ior(self, link=None):
        return self.input("IOR", link)

    def in_metallic(self, link=None):
        return self.input("Metallic", link)

    def in_normal(self, link=None):
        return self.input("Normal", link)

    def in_roughness(self, link=None):
        return self.input("Roughness", link)

    def in_sheen(self, link=None):
        return self.input("Sheen", link)

    def in_sheen_tint(self, link=None):
        return self.input("Sheen Tint", link)

    def in_specular(self, link=None):
        return self.input("Specular", link)

    def in_specular_tint(self, link=None):
        return self.input("Specular Tint", link)

    def in_subsurface(self, link=None):
        return self.input("Subsurface", link)

    def in_subsurface_color(self, link=None):
        return self.input("Subsurface Color", link)

    def in_subsurface_radius(self, link=None):
        return self.input("Subsurface Radius", link)

    def in_tangent(self, link=None):
        return self.input("Tangent", link)

    def in_transmission(self, link=None):
        return self.input("Transmission", link)

    def in_transmission_roughness(self, link=None):
        return self.input("Transmission Roughness", link)
