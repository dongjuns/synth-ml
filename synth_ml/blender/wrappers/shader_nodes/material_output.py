from .shader_node import ShaderNode


class MaterialOutput(ShaderNode):
    def __init__(self, node_tree):
        super().__init__(node_tree, "OutputMaterial")

    def in_surface(self, link=None):
        return self.input("Surface", link)

    def in_volume(self, link=None):
        return self.input("Volume", link)

    def in_displacement(self, link=None):
        return self.input("Displacement", link)
