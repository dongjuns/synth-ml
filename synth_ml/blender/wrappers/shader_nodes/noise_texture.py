from .shader_node import ShaderNode


class NoiseTexture(ShaderNode):
    def __init__(self, node_tree):
        super().__init__(node_tree, "TexNoise")

    @property
    def dimensions(self):
        return int(self().noise_dimensions[0])

    @dimensions.setter
    def dimensions(self, d: int):
        assert 1 <= d <= 4
        self().noise_dimensions = "{}D".format(d)

    # in
    def in_detail(self, link=None):
        return self.input("Detail", link)

    def in_distortion(self, link=None):
        return self.input("Distortion", link)

    def in_scale(self, link=None):
        return self.input("Scale", link)

    def in_vector(self, link=None):
        return self.input("Vector", link)

    def in_w(self, link=None):
        return self.input("W", link)

    # out
    def out_color(self, link=None):
        return self.output("Color", link)

    def out_fac(self, link=None):
        return self.output("Fac", link)
