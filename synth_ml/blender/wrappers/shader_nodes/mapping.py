from .shader_node import ShaderNode


class Mapping(ShaderNode):
    POINT = 'POINT'
    TEXTURE = 'TEXTURE'
    VECTOR = 'VECTOR'
    NORMAL = 'NORMAL'

    def __init__(self, node_tree, type: str = POINT):
        super().__init__(node_tree, 'Mapping')
        self.type = type

    @property
    def type(self):
        return self().vector_type

    @type.setter
    def type(self, type: str):
        self().vector_type = type

    # in

    def in_vector(self, link=None):
        return self.input('Vector', link)

    def in_location(self, link=None):
        return self.input('Location', link)

    def in_rotation(self, link=None):
        return self.input('Rotation', link)

    def in_scale(self, link=None):
        return self.input('Scale', link)

    # out

    def out_vector(self, link=None):
        return self.output('Vector', link)
