from .shader_node import ShaderNode


class MixRGB(ShaderNode):
    VALUE = "VALUE"
    COLOR = "COLOR"
    SATURATIO = "SATURATION"
    HUE = "HUE"

    DIVIDE = "DIVIDE"
    SUBTRACT = "SUBTRACT"
    DIFFERENCE = "DIFFERENCE"

    LINEAR_LIGHT = "LINEAR_LIGHT"
    SOFT_LIGHT = "SOFT_LIGHT"
    OVERLAY = "OVERLAY"

    ADD = "ADD"
    COLOR_DODGE = "DODGE"
    SCREEN = "SCREEN"
    LIGHTEN = "LIGHTEN"

    COLOR_BURN = "BURN"
    MULTIPLY = "MULTIPLY"
    DARKEN = "DARKEN"

    MIX = "MIX"

    def __init__(self, node_tree, blend_type=MIX, fac=1., color_1=(1., 1., 1., 1.), color_2=(1., 1., 1., 1.),
                 use_clamp=False):
        super().__init__(node_tree, "MixRGB")
        self().blend_type = blend_type
        self().use_clamp = use_clamp
        self.in_fac(fac)
        self.in_color_1(color_1)
        self.in_color_2(color_2)

    @property
    def blend_type(self):
        return self().blend_type

    @blend_type.setter
    def blend_type(self, blend_type: str):
        self().blend_type = blend_type

    @property
    def use_clamp(self):
        return self().use_clamp

    @use_clamp.setter
    def use_clamp(self, use_clamp: bool):
        self().use_clamp = use_clamp

    # in
    def in_fac(self, link=None):
        return self.input("Fac", link)

    def in_color_1(self, link=None):
        return self.input("Color1", link)

    def in_color_2(self, link=None):
        return self.input("Color2", link)

    # out
    def out_color(self, link=None):
        return self.output("Color", link)
