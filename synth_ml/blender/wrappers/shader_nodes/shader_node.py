from ..nodes import Node


class ShaderNode(Node):
    def __init__(self, obj_with_node_tree, shader_node_type: str):
        super().__init__(obj_with_node_tree, "ShaderNode" + shader_node_type)
