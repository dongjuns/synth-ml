from .shader_node import ShaderNode


class TextureCoordinate(ShaderNode):
    def __init__(self, node_tree):
        super().__init__(node_tree, "TexCoord")

    def out_camera(self, link=None):
        return self.output("Camera", link)

    def out_generated(self, link=None):
        return self.output("Generated", link)

    def out_normal(self, link=None):
        return self.output("Normal", link)

    def out_object(self, link=None):
        return self.output("Object", link)

    def out_reflection(self, link=None):
        return self.output("Reflection", link)

    def out_uv(self, link=None):
        return self.output("UV", link)

    def out_window(self, link=None):
        return self.output("Window", link)
