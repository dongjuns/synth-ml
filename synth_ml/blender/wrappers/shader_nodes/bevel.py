from .shader_node import ShaderNode


class Bevel(ShaderNode):
    def __init__(self, node_tree):
        super().__init__(node_tree, "Bevel")

    @property
    def samples(self):
        return self().samples

    @samples.setter
    def samples(self, samples: int):
        self().samples = samples

    # in
    def in_radius(self, link=None):
        return self.input("Radius", link)

    def in_normal(self, link=None):
        return self.input("Normal", link)

    # out
    def out_normal(self, link=None):
        return self.output("Normal", link)
