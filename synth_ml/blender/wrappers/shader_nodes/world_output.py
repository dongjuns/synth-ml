from .shader_node import ShaderNode


class WorldOutput(ShaderNode):
    # target
    ALL = 'ALL'
    CYCLES = 'CYCLES'
    EEVEE = 'EEVEE'

    def __init__(self, node_tree, target=ALL):
        super().__init__(node_tree, 'OutputWorld')
        self.target = target

    @property
    def target(self):
        return self().target

    @target.setter
    def target(self, target: str):
        self().target = target

    def in_surface(self, link=None):
        return self.input('Surface', link)

    def in_volume(self, link=None):
        return self.input('Volume', link)
