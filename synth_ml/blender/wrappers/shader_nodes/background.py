from .shader_node import ShaderNode


class Background(ShaderNode):
    def __init__(self, node_tree, strength=1.):
        super().__init__(node_tree, 'Background')
        self.in_strength(strength)

    def in_color(self, link=None):
        return self.input('Color', link)

    def in_strength(self, link=None):
        return self.input('Strength', link)

    def out_background(self, link=None):
        return self.output('Background', link)
