# constraint types
TRACK_TO = "TRACK_TO"

TRACK_X = "TRACK_X"
TRACK_Y = "TRACK_Y"
TRACK_Z = "TRACK_Z"
TRACK_NEGATIVE_X = "TRACK_NEGATIVE_X"
TRACK_NEGATIVE_Y = "TRACK_NEGATIVE_Y"
TRACK_NEGATIVE_Z = "TRACK_NEGATIVE_Z"

UP_X = "UP_X"
UP_Y = "UP_Y"
UP_Z = "UP_Z"
UP_NEGATIVE_X = "UP_NEGATIVE_X"
UP_NEGATIVE_Y = "UP_NEGATIVE_Y"
UP_NEGATIVE_Z = "UP_NEGATIVE_Z"
