from typing import Union
from pathlib import Path

from ..scene import Scene
from .compositor_node import CompositorNode


class FileOutput(CompositorNode):
    # File Format
    JPEG = "JPEG"
    PNG = "PNG"
    OPEN_EXR = "OPEN_EXR"

    # Color Mode
    BW = "BW"
    RGB = "RGB"
    RGBA = "RGBA"

    # Color Depth
    eight = '8'
    sixteen = '16'
    thirty_two = '32'

    def __init__(self, scene: Scene, folder, file_format=PNG, color_mode=RGB, color_depth=None, image=None):
        super().__init__(scene, "OutputFile")
        self().file_slots[0].path = ""
        self.base_path = folder
        self.file_format = file_format
        self.color_mode = color_mode
        if color_depth:
            self.color_depth = color_depth
        if image:
            self.in_image(image)

    @property
    def base_path(self):
        return self().base_path

    @base_path.setter
    def base_path(self, base_path: Union[Path, str]):
        folder = Path(base_path)
        folder.mkdir(exist_ok=True)
        self().base_path = str(folder.absolute())

    @property
    def file_format(self):
        return self().format.file_format

    @file_format.setter
    def file_format(self, file_format: str):
        self().format.file_format = file_format

    @property
    def color_mode(self):
        return self().format.color_mode

    @color_mode.setter
    def color_mode(self, color_mode: str):
        self().format.color_mode = color_mode

    @property
    def color_depth(self):
        return self().format.color_depth

    @color_depth.setter
    def color_depth(self, color_depth: str):
        self().format.color_depth = color_depth

    @property
    def quality(self):
        return self().format.quality

    @quality.setter
    def quality(self, quality: int):
        self().format.quality = quality

    @property
    def compression(self):
        return self().format.compression

    @compression.setter
    def compression(self, compression: int):
        self().format.compression = compression

    def in_image(self, link=None):
        return self.input(0, link)
