from ..scene import Scene
from .compositor_node import CompositorNode


class RenderLayers(CompositorNode):
    def __init__(self, scene: Scene):
        super().__init__(scene, 'RLayers')

    def out_image(self, link=None):
        return self.output('Image', link)

    def out_alpha(self, link=None):
        return self.output('Alpha', link)

    def out_depth(self, link=None):
        return self.output('Depth', link)

    def out_normal(self, link=None):
        return self.output('Normal', link)

    def out_uv(self, link=None):
        return self.output('UV', link)

    def out_vector(self, link=None):
        return self.output('Vector', link)

    def out_shadow(self, link=None):
        return self.output('Shadow', link)

    def out_ao(self, link=None):
        return self.output('AO', link)

    def out_index_ob(self, link=None):
        return self.output('IndexOB', link)

    def out_index_ma(self, link=None):
        return self.output('IndexMA', link)

    def out_mist(self, link=None):
        return self.output('Mist', link)

    def out_emit(self, link=None):
        return self.output('Emit', link)

    def out_env(self, link=None):
        return self.output('Env', link)

    def out_diff_dir(self, link=None):
        return self.output('DiffDir', link)

    def out_diff_ind(self, link=None):
        return self.output('DiffInd', link)

    def out_diff_col(self, link=None):
        return self.output('DiffCol', link)

    def out_gloss_dir(self, link=None):
        return self.output('GlossDir', link)

    def out_gloss_ind(self, link=None):
        return self.output('GlossInd', link)

    def out_gloss_col(self, link=None):
        return self.output('GlossCol', link)

    def out_trans_dir(self, link=None):
        return self.output('TransDir', link)

    def out_trans_ind(self, link=None):
        return self.output('TransInd', link)

    def out_trans_col(self, link=None):
        return self.output('TransCol', link)

    def out_subsurface_dir(self, link=None):
        return self.output('SubsurfaceDir', link)

    def out_subsurface_ind(self, link=None):
        return self.output('SubsurfaceInd', link)

    def out_subsurface_col(self, link=None):
        return self.output('SubsurfaceCol', link)
