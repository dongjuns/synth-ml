from ..nodes import Node
from ..scene import Scene


class CompositorNode(Node):
    def __init__(self, scene: Scene, shader_node_type: str):
        super().__init__(scene, "CompositorNode" + shader_node_type)
