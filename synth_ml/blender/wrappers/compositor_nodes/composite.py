from ..scene import Scene
from .compositor_node import CompositorNode


class Composite(CompositorNode):
    def __init__(self, scene: Scene):
        super().__init__(scene, "Composite")

    @property
    def use_alpha(self):
        return self().use_alpha

    @use_alpha.setter
    def use_alpha(self, use_alpha: bool):
        self().use_alpha = use_alpha

    def in_image(self, link=None):
        return self.input('Image', link)

    def in_alpha(self, link=None):
        return self.input('Alpha', link)

    def in_z(self, link=None):
        return self.input('Z', link)
