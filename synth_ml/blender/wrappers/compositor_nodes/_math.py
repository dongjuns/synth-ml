from ..scene import Scene
from .compositor_node import CompositorNode


class Math(CompositorNode):
    ADD = "ADD"
    SUBTRACT = "SUBTRACT"
    MULTIPLY = "MULTIPLY"
    DIVIDE = "DIVIDE"

    POWER = "POWER"
    LOGARITHM = "LOGARITHM"
    SQUARE_ROOT = "SQRT"
    ABSOLUTE = "ABSOLUTE"

    MINIMUM = "MINIMUM"
    MAXIMUM = "MAXIMUM"
    LESS_THAN = "LESS_THAN"
    GREATER_THAN = "GREATER_THAN"

    ROUND = "ROUND"
    FLOOR = "FLOOR"
    CEIL = "CEIL"
    FRACTION = "FRACT"
    MODULO = "MODULO"

    SINE = "SINE"
    COSINE = "COSINE"
    TANGENT = "TANGENT"
    ARCSINE = "ARCSINE"
    ARCCOSINE = "ARCCOSINE"
    ARCTANGENT = "ARCTANGENT"
    ARCTAN2 = "ARCTAN2"

    def __init__(self, scene: Scene, operation=ADD, value_1=1., value_2=1., use_clamp=False):
        super().__init__(scene, "Math")
        self.operation = operation
        self.use_clamp = use_clamp
        self.in_value_1(value_1)
        self.in_value_2(value_2)

    @property
    def operation(self):
        return self().operation

    @operation.setter
    def operation(self, operation: str):
        self().operation = operation

    @property
    def use_clamp(self):
        return self().use_clamp

    @use_clamp.setter
    def use_clamp(self, use_clamp: bool):
        self().use_clamp = use_clamp

    def in_value_1(self, link=None):
        return self.input(0, link)

    def in_value_2(self, link=None):
        return self.input(1, link)

    def out_value(self):
        return self.output('Value')
