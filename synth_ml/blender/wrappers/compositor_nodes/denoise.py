from ..scene import Scene
from .compositor_node import CompositorNode


class Denoise(CompositorNode):
    def __init__(self, scene: Scene, image=None, normal=None, albedo=None):
        super().__init__(scene, "Denoise")
        if image:
            self.in_image(image)
        if normal:
            self.in_normal(normal)
        if albedo:
            self.in_albedo(albedo)

    @property
    def use_hdr(self):
        return self().use_hdr

    @use_hdr.setter
    def use_hdr(self, use_hdr: bool):
        self().use_hdr = use_hdr

    def in_image(self, link=None):
        return self.input('Image', link)

    def in_normal(self, link=None):
        return self.input('Normal', link)

    def in_albedo(self, link=None):
        return self.input('Albedo', link)

    def out_image(self, link=None):
        return self.output('Image', link)
