import sys
import argparse
from pathlib import Path


def get_args():
    all_args = sys.argv
    split_a = all_args.index('--', 0)
    split_b = len(all_args)
    try:
        split_b = all_args.index('--', split_a + 1)
    except:
        pass
    blender_args = all_args[:split_a]
    synth_ml_args = all_args[split_a + 1:split_b]
    user_args = all_args[split_b + 1:]
    return blender_args, synth_ml_args, user_args


def get_blender_args():
    return get_args()[0]


def get_synth_ml_args():
    return get_args()[1]


def get_user_args():
    return get_args()[2]


def parse_synth_ml_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-sys-path")
    parser.add_argument("-compute-devices")
    parser.add_argument("-script-filepath")
    return parser.parse_args(get_synth_ml_args())
