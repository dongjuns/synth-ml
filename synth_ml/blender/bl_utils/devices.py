import sys

# Not all features are implemented for optix
CUDA = "CUDA"
OPENCL = "OPENCL"


def get_cycles_preferences():
    import bpy
    return bpy.context.preferences.addons['cycles'].preferences


def get_devices():
    import bpy
    for device_type in get_cycles_preferences().get_device_types(bpy.context):
        get_devices_for_type(device_type[0])
    return get_cycles_preferences().get_devices()


def get_devices_for_type(device_type):
    return get_cycles_preferences().get_devices_for_type(device_type)


def enable_compute_devices(desired_device_ids, verbose=False):
    all_devices = get_devices()[0]

    if verbose:
        print("\nFound devices:", all_devices)
    if max(desired_device_ids) >= len(all_devices):
        print("Error: compute device id is out of range:", max(desired_device_ids))
        sys.exit(1)

    desired_devices = set([all_devices[idx] for idx in desired_device_ids])
    for device in all_devices:
        device.use = device in desired_devices

    # find the best compute type that allows all desired devices to be enabled
    success = False
    for compute_device_type in (CUDA, OPENCL):
        available_devices = set(get_devices_for_type(compute_device_type))
        used_devices = desired_devices & available_devices
        if used_devices < desired_devices:
            continue
        get_cycles_preferences().compute_device_type = compute_device_type
        if verbose:
            print("Using compute devices:", desired_devices)
            print("Using compute device type:", compute_device_type)
            print()
        success = True
        break
    assert success, "could not find compute device type that allows all desired devices to be enabled"
