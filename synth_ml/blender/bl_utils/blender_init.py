import sys
from pathlib import Path
import numpy as np  # fixes a weird numpy import error

sys.path.append(str(Path(__file__).parent.parent.parent.parent))

from synth_ml.blender.bl_utils.devices import enable_compute_devices
from synth_ml.blender.bl_utils.redirect import hard_redirect_blender_stdout, clean_redirect_files
from synth_ml.blender.bl_utils.arg_parser import parse_synth_ml_args

args = parse_synth_ml_args()
sys.path = args.sys_path.split(":") + sys.path

compute_devices = args.compute_devices[1:]
if compute_devices:
    device_ids = [int(idx) for idx in compute_devices.split(":")]
    enable_compute_devices(device_ids, verbose=True)

clean_redirect_files()
print("Redirecting blender stdout to blender_c.log and blender_py.log\n")
hard_redirect_blender_stdout()
