import os
import sys
from pathlib import Path
from contextlib import redirect_stdout

# some of the blender printout is from c, which cannot be redirected using the redirectstdout,
# and some of the blender printout is in python which prints to sys.stdout

hard_redirect_fp = Path("blender_c.log")
soft_redirect_fp = Path("blender_py.log")


def clean_redirect_files():
    for log in hard_redirect_fp, soft_redirect_fp:
        if log.exists():
            log.unlink()


def hard_redirect_blender_stdout():
    log_file_path = Path(hard_redirect_fp)
    if log_file_path.exists():
        log_file_path.unlink()
    log_file_path.open('w').close()

    old_fd = os.dup(1)
    sys.stdout.flush()
    os.close(1)
    assert os.open(str(log_file_path), os.O_WRONLY) == 1
    sys.stdout = os.fdopen(old_fd, 'w')


class BlenderRedirect:
    file = None
    redirect = None

    def __enter__(self):
        self.file = soft_redirect_fp.open('a')
        self.file.__enter__()
        self.redirect = redirect_stdout(self.file)
        self.redirect.__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.redirect.__exit__(exc_type, exc_val, exc_tb)
        self.file.__exit__(exc_type, exc_val, exc_tb)
