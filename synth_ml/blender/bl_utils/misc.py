from pathlib import Path
import os
from .arg_parser import parse_synth_ml_args

CYCLES = "CYCLES"
EEVEE = "BLENDER_EEVEE"
WORKBENCH = "BLENDER_WORKBENCH"
RENDER_ENGINES = (CYCLES, EEVEE, WORKBENCH)


def get_default_output_folder(taskName="", dateTime="",  directoryName=""):
    script_fp = Path(parse_synth_ml_args().script_filepath)
    output_folder = script_fp.parent/"synth_ml_data"/taskName/dateTime/directoryName
    currentDirectory = os.getcwd()
    newDirectoryPath = os.path.join(currentDirectory, output_folder)
    isExist = os.path.exists(newDirectoryPath)
    newDirectoryPath = str(newDirectoryPath)
    if not isExist:
        os.makedirs(newDirectoryPath, exist_ok=True)
    
    return output_folder
