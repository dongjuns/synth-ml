# synth-ml

**synth-ml** is a python module that aims to support rapid development of machine learning on synthetic data.
Currently **synth-ml** consists of 
a script `synth-ml-blender`, 
some wrappers around blender types, 
and a set of helper functions
that enable easy *domain randomization* of material properties, object transforms and hdri environments.

Pull requests are welcome.

### install:
linux and macos only
```
git clone https://github.com/RasmusHaugaard/synth-ml.git
cd synth-ml
pip install -e .
```

### hello world:
```python
# examples/hello_world/hello_world.py
from synth_ml.blender.wrappers import Scene, Camera, Object
from synth_ml.blender import callbacks as cb, materials
from synth_ml.objs import get_objs_folder

s = Scene()

obj = Object.import_obj(get_objs_folder() / "suzanne.obj", scene=s)[0]  # file contains one object
tripod = Object(scene=s).look_at((0, 0, 0))
cam = Camera(scene=s, parent=tripod)

material = materials.ProceduralNoiseVoronoiMix()
obj.assign_material(material)

ts = cb.TransformSampler()
ts.pos_sphere_volume(obj, center=[0, 0, 0], radius=1)
ts.pos_sphere_shell(obj=tripod, center=[3, -3, 0], r_min=0.5, r_max=1.)
ts.rot_axis_uniform(cam, 'z', -.3, .3)

s.callback = cb.CallbackCompose(
    cb.Renderer(
        scene=s, resolution=256, engines='CYCLES', cycles_samples=10,
        denoise=True, depth=True, normal=True
    ),
    cb.MetadataLogger(scene=s, objects=[obj]),
    ts,
    material.sampler_cb(p_metallic=0.8),
    cb.HdriEnvironment(scene=s),
)

s.render_frames(range(10))
```

```
$ cd synth-ml/examples/hello_world
$ synth-ml-blender hello_world.py --compute-devices 0
```
cycles
![](examples/hello_world/cycles.png)
cycles denoise
![](examples/hello_world/cycles_denoise.png)
depth
![](examples/hello_world/depth.png)
normals (world coordinates)
![](examples/hello_world/normal_world.png)

The metadata logger saves camera- and object data and has a few helpers for common transforms.
```python
# examples/hello_world/mark_center.py
import cv2
from pathlib import Path
from synth_ml.blender.callbacks.metadata import Metadata

Path("center_marked").mkdir(exist_ok=True)
for i in range(10):
    md = Metadata("synth_ml_data/metadata/{:04}.json".format(i))
    bgr = cv2.imread("synth_ml_data/cycles_denoise/{:04}.png".format(i))
    center = md.world_2_image(md.objects['Suzanne'].position)
    cv2.drawMarker(bgr, tuple(center[:2]), (0, 0, 255), cv2.MARKER_CROSS, 15, 2)
    cv2.imwrite("center_marked/{:04}.png".format(i), bgr)
``` 
![](examples/hello_world/center_marked.png)
```python
# examples/hello_world/mark_bbox.py
import cv2
import trimesh
from pathlib import Path
from synth_ml.blender.callbacks.metadata import Metadata
from synth_ml.objs import get_objs_folder

obj = trimesh.load(get_objs_folder() / "suzanne.obj")  # type: trimesh.Trimesh
vertices = obj.vertices.T

Path("bbox_marked").mkdir(exist_ok=True)
for i in range(10):
    md = Metadata("synth_ml_data/metadata/{:04}.json".format(i))
    xmin, ymin, xmax, ymax = md.bbox(vertices, md.objects['Suzanne'].t_world)
    bgr = cv2.imread("synth_ml_data/cycles_denoise/{:04}.png".format(i))
    cv2.rectangle(bgr, (xmin, ymin), (xmax, ymax), (0, 0, 255), 2)
    cv2.imwrite("bbox_marked/{:04}.png".format(i), bgr)
```
![](examples/hello_world/bbox_marked.png)
### using an existing scene
See `examples/peg_in_hole/peg_in_hole.py` for the full example.
```python
from synth_ml.blender.wrappers import Scene, Object, Camera
s = Scene.from_name('Scene')
cube = Object.from_name('Cube')
peg = Object.from_name('Peg')
#...
```
```
$ cd examples/peg_in_hole
$ synth-ml-blender peg_in_hole.py --blend-file peg_in_hole.blend -- --frame-end 7
```

![](examples/peg_in_hole/peg_in_hole.png)

### synth-ml-blender
On first run, 
    on linux, the script will download blender 2.81a on linux.
    On mac, it will look in the applications folder and check if the version is 2.81a.
It then 
    starts a blender instance in background mode, 
    enables chosen compute devices, 
    injects your python paths,
    redirects blender output to a separate file 
    and runs your script.
```
$ synth-ml-blender -h
usage: synth-ml-blender [-h] [--foreground] [--blend-file BLEND_FILE]
                        [--compute-devices COMPUTE_DEVICES [COMPUTE_DEVICES ...]]
                        script_filepath

positional arguments:
  script_filepath

optional arguments:
  -h, --help            show this help message and exit
  --foreground          force blender to open in foreground mode (wont respond
                        before after script)
  --blend-file BLEND_FILE
                        open a blend file before execution of script
  --compute-devices COMPUTE_DEVICES [COMPUTE_DEVICES ...]
                        indices of compute devices to use for rendering
```

### how to cite?
```
@software{synth-ml,
	author = {{Rasmus Laurvig Haugaard}},
	title = {synth-ml},
	url = {https://github.com/RasmusHaugaard/synth-ml},
	version = {0.0.1},
	date = {2020-01-30},
}
```